package ir.fp.project1.constant;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

public class MyConstants {

	// DEFAULT PAGE SIZE //
	public final static Integer DEFAULT_PAGE_SIZE = 1000;
	public final static String STR_DEFAULT_PAGE_SIZE = "1000";
	
	// MY CODES //
	public final static String ERROR = "0";
	public final static String OK = "1";
	public final static String NOT_OK = "2";
	public final static String FORBIDDEN = "3";
	public final static String NOT_FOUND = "4";
	
	// FOR SEARCHS //
	public class SEARCHE_KEYS {

		// CARD //
		public final static String CARD__ALL_FOR_SEARCH = "CARD__ALL";
		public final static String CARD__UUID_FOR_SEARCH = "CARD__UUID";
		public final static String CARD__OWNERNAME_FOR_SEARCH = "CARD__OWNERNAME";
		public final static String CARD__PAN_FOR_SEARCH = "CARD__PAN";
		
	}
}
