package ir.fp.project1.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ir.fp.project1.constant.MyConstants;
import ir.fp.project1.dto.MyResponseDto;
import ir.fp.project1.dto.TransferDto;
import ir.fp.project1.model.Card;
import ir.fp.project1.repository.CardRepository;
import ir.fp.project1.util.MyUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Slf4j
@Service
public class CardService {

	private final CardRepository cardRepository;

	private final PasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public CardService(CardRepository cardRepository, PasswordEncoder bCryptPasswordEncoder) {
		this.cardRepository = cardRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public Card findById(Long id) {
		Optional<Card> opCard = cardRepository.findById(id);
		try {
			return opCard.get();
		} catch (Exception e) {
			return null;
		}
	}

	public Card findByUuid(String uuid) {
		return cardRepository.findByUuid(uuid);
	}

	public List<Card> findAll() {
		return cardRepository.findAll();
	}

	public Page<Card> findAll(Pageable pageable) {
		return cardRepository.findAll(pageable);
	}

	public Card findByOwnerName(String ownerName) {
		return cardRepository.findByOwnerName(ownerName);
	}

	public Page<Card> searchByUuidWithLike(Pageable pageable, String uuid) {
		return cardRepository.searchByUuidWithLike(pageable, uuid);
	}

	public Page<Card> searchByOwnerNameWithLike(Pageable pageable, String ownerName) {
		return cardRepository.searchByOwnerNameWithLike(pageable, ownerName);
	}

	public Page<Card> searchByPanWithLike(Pageable pageable, String pan) {
		return cardRepository.searchByPanWithLike(pageable, pan);
	}

	public Card saveCard(Card card) {
		card.setPin(bCryptPasswordEncoder.encode(card.getPin()));
		return cardRepository.save(card);
	}

	public void deleteCard(Card card) {
		cardRepository.delete(card);
	}

	public void deleteAllInBatch() {
		cardRepository.deleteAllInBatch();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	@Transactional
	public MyResponseDto iDoCard2Card(TransferDto inputTransferDto) throws Exception {
		final Card sourceCardFromDb = cardRepository.findByPan(inputTransferDto.getSource());
		final Card destinationCardFromDb = cardRepository.findByPan(inputTransferDto.getDest());

		final Integer expYear = Integer.valueOf(inputTransferDto.getExpDate().substring(0, 1)) < 99
				? Integer.valueOf("14" + inputTransferDto.getExpDate().substring(0, 1))
				: Integer.valueOf("13" + inputTransferDto.getExpDate().substring(0, 1));
		final Integer expMonth = Integer.valueOf(inputTransferDto.getExpDate().substring(2, 3));

		if (!MyUtil.checkIfExpDateExpired(expYear, expMonth)
				&& sourceCardFromDb.getCvv2().equalsIgnoreCase(inputTransferDto.getCvv2())
				&& bCryptPasswordEncoder.encode(sourceCardFromDb.getPin())
						.equalsIgnoreCase(bCryptPasswordEncoder.encode(inputTransferDto.getPin()))) {
			if (sourceCardFromDb.getDeposit() >= inputTransferDto.getAmount()) {
				sourceCardFromDb.setDeposit(sourceCardFromDb.getDeposit() - inputTransferDto.getAmount());
				destinationCardFromDb.setDeposit(destinationCardFromDb.getDeposit() + inputTransferDto.getAmount());
				cardRepository.save(sourceCardFromDb);
				cardRepository.save(destinationCardFromDb);
				try {
					// Send SMS To Receiver //
				} catch (Exception e) {
					log.warn("SMS Not Sent");
				}
				return new MyResponseDto(MyConstants.OK, "Successfull Transaction");
			} else {
				// Deposit is not enough //
				return new MyResponseDto(MyConstants.NOT_OK, "Deposit is not Enough");
			}
		}
		throw new Exception("Shiit Happen Sometimes");
	}
}
