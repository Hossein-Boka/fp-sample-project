package ir.fp.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ir.fp.project1.model.Card;
import ir.fp.project1.service.CardService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SampleData {

	private final CardService cardService;
	
	@Autowired
	public SampleData(CardService cardService) {
		this.cardService = cardService;
	}
	
	// Truncate Tables //
	public void truncateTables() {
		cardService.deleteAllInBatch();
	}

	// Insert Initial Data //
	public void insertInitialData() {
		Card newCard1 = new Card();
		newCard1.setOwnerName("Hossein Boka");
		newCard1.setPan("1234-1234-1234-1234");
		newCard1.setCvv2("1111");
		newCard1.setExpYear(1402);
		newCard1.setExpMonth(8);
		newCard1.setPin("123456");
		newCard1.setDeposit(100000);
		newCard1 = cardService.saveCard(newCard1);
	
		Card newCard2 = new Card();
		newCard2.setOwnerName("Hossein Boka");
		newCard2.setPan("4321-4321-4321-4321");
		newCard2.setCvv2("2222");
		newCard2.setExpYear(1405);
		newCard2.setExpMonth(11);
		newCard2.setPin("987654");
		newCard2.setDeposit(200000);
		newCard2 = cardService.saveCard(newCard2);
		
	}
	
}
