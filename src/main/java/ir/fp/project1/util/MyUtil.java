package ir.fp.project1.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

public class MyUtil {
	public static String newGeneratedUuidWith24Character() {
		return UUID.randomUUID().toString().replace("-", "").substring(2, 26).toLowerCase();
	}

	public static LocalTime currentTime() {
		return LocalTime.now();
	}

	public static LocalDateTime currentDateTime() {
		return LocalDateTime.now();
	}

	public static boolean checkIfExpDateExpired(Integer expYear, Integer expMonth) {
		if (LocalDate.now().getYear() > expYear) {
			return true;
		} else if (LocalDate.now().getYear() == expYear && LocalDate.now().getMonth().getValue() > expMonth) {
			return true;
		} else {
			return false;
		}
	}
}
