package ir.fp.project1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ir.fp.project1.constant.MyConstants;
import ir.fp.project1.dto.MyResponseDto;
import ir.fp.project1.dto.TransferDto;
import ir.fp.project1.service.CardService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1")
public class PaymentController {

	private final CardService cardService;

	@Autowired
	public PaymentController(CardService cardService) {
		this.cardService = cardService;
	}

	// ############ //
	// ### POST ### //
	// ############ //

	/**
	 * @category POST
	 * @apiNote For Card to Card 
	 * @param inputTransferDto
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@PostMapping(value = "/payment/transfer", // # PLACE_HOLDER # //
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> payment‬‬‪Provider‬‬‫‪1Api‬‬( // [PH]
			@RequestBody TransferDto inputTransferDto // [PH]
	) {
		try {
			inputTransferDto.setExpDate(inputTransferDto.getExpDate().replace("/", ""));
			final MyResponseDto myResponseDto = cardService.iDoCard2Card(inputTransferDto);
			if (myResponseDto.getCode() == MyConstants.OK) {
				return new ResponseEntity<>(myResponseDto, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(myResponseDto, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			log.error("Exception in [PaymentController.payment‬‬‪Provider‬‬‫‪1Api‬‬()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
