package ir.fp.project1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ir.fp.project1.constant.MyConstants;
import ir.fp.project1.dto.MyResponseDto;
import ir.fp.project1.model.Card;
import ir.fp.project1.service.CardService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Slf4j
@RestController
@RequestMapping(value = "/api/v1")
public class CardController {

	private final CardService cardService;

	@Autowired
	public CardController(CardService cardService) {
		this.cardService = cardService;
	}

	// ########### //
	// ### GET ### //
	// ########### //
	
	/**
	 * @category GET
	 * @apiNote Fetch All Cards
	 * @param pageNumber
	 * @param pageSize
	 * @param sortColumn
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@GetMapping(value = "/card/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> fetchAllCards( // [PH]
			@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber, // [PH]
			@RequestParam(value = "pageSize", defaultValue = MyConstants.STR_DEFAULT_PAGE_SIZE) Integer pageSize, // [PH]
			@RequestParam(value = "sortColumn", defaultValue = "id") String sortColumn // [PH]
	) {
		try {
			final Page<Card> cardsFromDb = cardService
					.findAll(PageRequest.of(pageNumber, pageSize, Sort.by(sortColumn).ascending()));
			return new ResponseEntity<>(cardsFromDb, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception in [CardController.fetchAllCards()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @category GET
	 * @apiNote Fetch Cards By Specific Fields
	 * @param pageNumber
	 * @param pageSize
	 * @param sortColumn
	 * @param searchField
	 * @param searchKey
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@GetMapping(value = "/card/search", // # PLACE_HOLDER # //
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> fetchCardsBySpecificFields( // [PH]
			@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber, // [PH]
			@RequestParam(value = "pageSize", defaultValue = MyConstants.STR_DEFAULT_PAGE_SIZE) Integer pageSize, // [PH]
			@RequestParam(value = "sortColumn", defaultValue = "id") String sortColumn, // [PH]
			@RequestParam(value = "searchField", defaultValue = MyConstants.SEARCHE_KEYS.CARD__ALL_FOR_SEARCH) String searchField, // [PH]
			@RequestParam(value = "searchKey", defaultValue = "") String searchKey // [PH]
	) {
		try {
			final Page<Card> cardsFromDb;
			switch (searchField) {
			case MyConstants.SEARCHE_KEYS.CARD__ALL_FOR_SEARCH:
				cardsFromDb = cardService
						.findAll(PageRequest.of(pageNumber, pageSize, Sort.by(sortColumn).ascending()));
				break;
			case MyConstants.SEARCHE_KEYS.CARD__UUID_FOR_SEARCH:
				cardsFromDb = cardService.searchByUuidWithLike(
						PageRequest.of(pageNumber, pageSize, Sort.by(sortColumn).ascending()), searchKey);
				break;
			case MyConstants.SEARCHE_KEYS.CARD__OWNERNAME_FOR_SEARCH:
				cardsFromDb = cardService.searchByOwnerNameWithLike(
						PageRequest.of(pageNumber, pageSize, Sort.by(sortColumn).ascending()), searchKey);
				break;
			case MyConstants.SEARCHE_KEYS.CARD__PAN_FOR_SEARCH:
				cardsFromDb = cardService.searchByPanWithLike(
						PageRequest.of(pageNumber, pageSize, Sort.by(sortColumn).ascending()), searchKey);
				break;
			default:
				///////////////////////////////////////////
				// TODO : REPORT A DESTRUCTIVE OPERATION //
				///////////////////////////////////////////
				throw new Exception("Problem with Definition of 'searchField'");
			}

			return new ResponseEntity<>(cardsFromDb, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception in [CardController.fetchCardsBySpecificFields()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// ############ //
	// ### POST ### //
	// ############ //

	/**
	 * @category POST
	 * @apiNote New Card
	 * @param inputCard
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@PostMapping(value = "/card/new", // # PLACE_HOLDER # //
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> newProduct( // [PH]
			@RequestBody Card inputCard // [PH]
	) {
		try {
			final Card savedCard = cardService.saveCard(inputCard);
			
			return new ResponseEntity<>(savedCard, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception in [CardController.newProduct()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// ########### //
	// ### PUT ### //
	// ########### //

	/**
	 * @category PUT
	 * @apiNote Edit Card
	 * @param cardUuid
	 * @param inputCard
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@PutMapping(value = "/card/{cardUuid}/edit", // # PLACE_HOLDER # //
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editCard( // [PH]
			@PathVariable("cardUuid") String cardUuid, // [PH]
			@RequestBody Card inputCard // [PH]
	) {
		try {
			final Card cardFromDb = cardService.findByUuid(cardUuid);
			if (cardFromDb == null) {
				///////////////////////////////////////////
				// TODO : REPORT A DESTRUCTIVE OPERATION //
				///////////////////////////////////////////
			}

			if (inputCard.getOwnerName() != null) {
				cardFromDb.setOwnerName(inputCard.getOwnerName());
			}
			if (inputCard.getPan() != null) {
				cardFromDb.setPan(inputCard.getPan());
			}
			if (inputCard.getCvv2() != null) {
				cardFromDb.setCvv2(inputCard.getCvv2());
			}
			if (inputCard.getExpYear() != null) {
				cardFromDb.setExpYear(inputCard.getExpYear());
			}
			if (inputCard.getExpMonth() != null) {
				cardFromDb.setExpMonth(inputCard.getExpMonth());
			}
			if (inputCard.getPin() != null) {
				cardFromDb.setPin(inputCard.getPin());
			}
			if (inputCard.getDeposit() != null) {
				cardFromDb.setDeposit(inputCard.getDeposit());
			}
			if (inputCard.getMobile() != null) {
				cardFromDb.setMobile(inputCard.getMobile());
			}
			
			final Card savedCard = cardService.saveCard(inputCard);

			return new ResponseEntity<>(savedCard, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception in [CardController.editCard()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// ############## //
	// ### DELETE ### //
	// ############## //

	/**
	 * @category DELETE
	 * @apiNote Delete Card
	 * @param cardUuid
	 * @return ResponseEntity
	 * @apiNote Test-Date =>
	 */
	@DeleteMapping(value = "/card/{cardUuid}/delete", // # PLACE_HOLDER # //
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteProduct( // [PH]
			@PathVariable("cardUuid") String cardUuid // [PH]
	) {
		try {
			final Card cardFromDb = cardService.findByUuid(cardUuid);
			if (cardFromDb == null) {
				///////////////////////////////////////////
				// TODO : REPORT A DESTRUCTIVE OPERATION //
				///////////////////////////////////////////MyConstants
			} else {
				cardService.deleteCard(cardFromDb);
			}
			
			return new ResponseEntity<>(new MyResponseDto(MyConstants.OK, "Deleted Successfully"), HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception in [CardController.deleteProduct()]");
			return new ResponseEntity<>(new MyResponseDto(MyConstants.ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
