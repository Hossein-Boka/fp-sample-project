package ir.fp.project1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class TransferDto {

	@JsonProperty
	private String source;
	
	@JsonProperty
	private String dest;
	
	@JsonProperty
	private String cvv2;
	
	@JsonProperty
	private String expDate;
	
	@JsonProperty
	private String pin;
	
	@JsonProperty
	private Integer amount;
	
}
