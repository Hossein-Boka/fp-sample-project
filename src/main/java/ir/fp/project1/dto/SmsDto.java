package ir.fp.project1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class SmsDto {

	@JsonProperty
	private String message;
	
	@JsonProperty
	private String mobile;
	
}
