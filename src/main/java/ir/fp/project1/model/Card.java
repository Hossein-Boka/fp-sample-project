package ir.fp.project1.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import ir.fp.project1.util.MyUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "card__table", indexes = { // # PLACE_HOLDER # //
		@Index(name = "index___card___card_uuid", columnList = "card_uuid", unique = true),
		@Index(name = "index___card___pan", columnList = "pan", unique = true) })
public class Card {

	@JsonIgnore
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "card_id_sequence", sequenceName = "card_id_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_id_sequence")
	@Column(name = "card_id", updatable = false)
	private Long id;

	@JsonProperty
	@Column(name = "card_uuid", length = 24, nullable = false, updatable = false)
	private String uuid;

	@JsonProperty
	@Column(name = "owner_name", length = 60)
	private String ownerName;
	
	@JsonProperty
	@Column(name = "pan", length = 30, nullable = false)
	private String pan;

	@JsonProperty
	@Column(name = "cvv2", length = 5, nullable = false)
	private String cvv2;

	@JsonProperty
	@Column(name = "exp_year", nullable = false)
	private Integer expYear;
	
	@JsonProperty
	@Column(name = "exp_month", nullable = false)
	private Integer expMonth;

	@JsonProperty
	@Column(name = "pin", length = 210, nullable = false)
	private String pin;

	@JsonProperty
	@Column(name = "deposit", nullable = false)
	private Integer deposit;
	
	@JsonProperty
	@Column(name = "mobile", length = 15)
	private String mobile;
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "create_timestamp", columnDefinition = "TIMESTAMP", nullable = false, updatable = false)
	private LocalDateTime createTimestamp;

	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "last_update_timestamp", columnDefinition = "TIMESTAMP", nullable = false)
	private LocalDateTime lastUpdateTimestamp;

	// ###################################################

	@PrePersist
	private void prePersistOperation() {
		if (this.uuid == null) {
			this.uuid = MyUtil.newGeneratedUuidWith24Character();
		}
		this.createTimestamp = MyUtil.currentDateTime();
		this.lastUpdateTimestamp = MyUtil.currentDateTime();
	}

	@PreUpdate
	private void preUpdateOperation() {
		this.lastUpdateTimestamp = MyUtil.currentDateTime();
	}

}
