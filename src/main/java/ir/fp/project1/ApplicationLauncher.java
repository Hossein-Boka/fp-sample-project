package ir.fp.project1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@SpringBootApplication
@PropertySources({ // # PLACE_HOLDER # //
	@PropertySource("classpath:server.properties"),
	@PropertySource("classpath:application-${spring.profiles.active}.properties")})
public class ApplicationLauncher {

	public static void main(String[] args) {
		// ### Starting Application ### //
		SpringApplication.run(ApplicationLauncher.class, args);
	}

}
