package ir.fp.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Slf4j
@Component
public class ApplicationStartupRunner implements ApplicationRunner {

	private final Boolean truncateAndFillWithSampleData;
	private final SampleData sampleData;
	
	@Autowired
	public ApplicationStartupRunner(@Value("${ir.fp.truncate-and-fill-with-sample-data}") Boolean truncateAndFillWithSampleData,SampleData sampleData) {
		this.truncateAndFillWithSampleData = truncateAndFillWithSampleData;
		this.sampleData = sampleData;
	}
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("into application runner");
		if (truncateAndFillWithSampleData) {
			sampleData.truncateTables();
			sampleData.insertInitialData();
		}
		log.info("out of application runner");
	}
	
}
