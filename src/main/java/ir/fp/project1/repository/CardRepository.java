package ir.fp.project1.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ir.fp.project1.model.Card;

/**
 * @author Hossein Boka <i@Ho3e.in>
 * 
 */

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

	public Optional<Card> findById(Long id);

	public Card findByUuid(String uuid);

	public List<Card> findAll();

	public Page<Card> findAll(Pageable pageable);

	public Card findByOwnerName(String ownerName);
	
	public Card findByPan(String pan);
	
	@Query("SELECT c FROM Card c WHERE c.uuid LIKE %:uuid%")
	public Page<Card> searchByUuidWithLike(Pageable pageable, @Param("uuid") String uuid);

	@Query("SELECT c FROM Card c WHERE c.ownerName LIKE %:ownerName%")
	public Page<Card> searchByOwnerNameWithLike(Pageable pageable, @Param("ownerName") String ownerName);
	
	@Query("SELECT c FROM Card c WHERE c.pan LIKE %:pan%")
	public Page<Card> searchByPanWithLike(Pageable pageable, @Param("pan") String pan);
}
